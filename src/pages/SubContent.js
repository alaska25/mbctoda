import { Link } from "react-router-dom";
import { Nav } from "react-bootstrap"; 
export default function SubContent(){

	return(
		<>	
			<button className="tablink1 fw-bold" style={{backgroundImage: 'linear-gradient(#045b8f,#045b8f)'}}><Nav.Link as={Link} to="/AboutUs" eventKey="/AboutUs">ABOUT US</Nav.Link>
			</button><br/>
			<button className="tablink2 fw-bold" style={{backgroundImage: 'linear-gradient(#044b8f,#044b8f)'}}><Nav.Link as={Link} to="/Driver_List" eventKey="/Driver_List">DRIVERS</Nav.Link>
			</button>
		</>
		)
}