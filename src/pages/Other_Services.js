import {Card, Col, Row} from "react-bootstrap";
import img12 from "./images/car.png";
import img13 from "./images/motor.png";
export default function Other_Services(){
	return(
		<>
		<div className="container h-100 p-3" id="services">
		<h2 className="text-center fw-bold">Other Services</h2>
			<Row xs={1} md={2} className="g-4">
			      {Array.from({ length: 1 }).map((_, idx) => (
			      <>
			        
			        <Col>
			          <Card>
			            <Card.Img variant="top" className="d-block w-100 card-image square border-bottom"
                    src={img13}
                    alt="Third slide" img-fluid/>
			          </Card>
			        </Col>
			        <Col>
			          <Card>
			            <Card.Img variant="top" className="d-block w-100 card-image square border-bottom"
                    src={img12}
                    alt="Third slide" img-fluid />
			          </Card>
			        </Col>

			       </>
			      ))}
			</Row>

		</div>
		</>
		)
}
