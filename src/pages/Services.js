import {Card, CardGroup, Button} from "react-bootstrap";
import {Link} from "react-router-dom";
export default function Services(){
	
	return(
		<>
		<div className="container h-100 p-3" id="services">
			<h1 className="text-center fw-bold">Services We Offer</h1>
			<div className="d-flex align-items-center justify-content-center">
			         <Button as={Link} to="/Driver_List" eventKey="/Driver_List" className="m-2 w-100 h-30 b-size" id="bg-color">
			         <h3>Book A Ride</h3>
			         <p>RideMED app is a ride hailing that connects commuters to available drivers via calls and messages.</p>
			         </Button>
			         <Button as={Link} to="/Driver_List" eventKey="/Driver_List" className="m-2 w-100 h-30" id="bg-color1">
			         <h3>Buy/Delivery Service</h3>
			         <p> Do you need to buy something? RideMED driver will be your personal shoppers to a public market and malls.</p>
			         </Button>
			 </div>
		</div>
		</>
		)
}