import React, { useState } from 'react';
import { Navigate, useNavigate, Link } from "react-router-dom";
import { Nav } from "react-bootstrap";
import { faGears, faCar, faMotorcycle, faBicycle, faTruck, faRightFromBracket } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import img20 from "./images/tri.png";
import Driver_List from "../pages/Driver_List";
export default function Dashboard(){
	return(
		<>
			<div id="sidebar" className="text-start">
			  <a href="#tricycle_content" active><FontAwesomeIcon icon={faTruck} className="ms-1"></FontAwesomeIcon>&nbsp;Tricycle</a>

			  <a href="#" ><FontAwesomeIcon icon={faMotorcycle}></FontAwesomeIcon>&nbsp;Motorcycle<br/>(Coming Soon)</a>
			  <a href="#"><FontAwesomeIcon icon={faCar}></FontAwesomeIcon>&nbsp; Car<br/>(Coming Soon)</a>


			  <Nav.Link  as={Link} to="/logout" eventKey="/logout" className="text-light"><FontAwesomeIcon icon={faRightFromBracket} className="ms-1"></FontAwesomeIcon>&nbsp;Logout</Nav.Link >

			</div>

			<div id="tricycle_content" className="container p-3">
			  <Driver_List />
			</div>
		</>
		)
}
