import { useState, useEffect, useContext } from "react";
import UserContext from "../UserContext";
import { Navigate, useNavigate, Link } from "react-router-dom";
import Swal from "sweetalert2";
import { Form, Button, Nav } from "react-bootstrap";
import { MDBCheckbox } from 'mdb-react-ui-kit';

export default function RegisterVehicle(){
	const {user} = useContext(UserContext);
	const navigate = useNavigate();

	// State hooks to store the values of the input fields
	const [vehicleType, setVehicleType] = useState("");
	const [makeOfVehicle, setMakeOfVehicle] = useState("");
	const [chassisNumber, setChassisNumber] = useState("");
	const [plateNo, setPlateNo] = useState("");

	//Function to simulate passenger registration
	function registerVehicle(e){
		//prevents the page redirection via form submit
		e.preventDefault();

		// Checking if the email is still available
		fetch(`${process.env.REACT_APP_API_URL}/vehicles/checkVehicle`,{
			method: "POST",
			headers:{
				"Content-Type":"application/json"
			},
			body: JSON.stringify({
				plateNo: plateNo
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				Swal.fire({
						title: "Duplicate Plate Number found",
						icon: "error",
						text: "Please try again later!"
					});
			}
			else{
				fetch(`${process.env.REACT_APP_API_URL}/vehicles/register`,{
					method: "POST",
					headers:{
						"Content-Type":"application/json"
					},
					body: JSON.stringify({
						vehicleType: vehicleType,
						makeOfVehicle: makeOfVehicle,
						chassisNumber: chassisNumber,
						plateNo: plateNo
					})
				})
				.then(res => res.json())
				.then(data => {

					if(data){
						// Clear input fields
						setVehicleType("");
						setMakeOfVehicle("");
						setChassisNumber("");
						setPlateNo("");

						Swal.fire({
							title: "Successfully Added to the System!",
							icon: "success"
						})

						navigate("/");
					}
					else{
						Swal.fire({
							title: "Something went wrong!",
							icon: "error",
							text: "Please try again later!"
						})
					}
				})
			}
		})
		// Clear input fields
		setVehicleType("");
		setMakeOfVehicle("");
		setChassisNumber("");
		setPlateNo("");
	}


	//State to determine whether submit button is enabled or not.
	const [isActive, setIsActive] = useState(false);

	// To enable the submit button:
		// No empty input fields.
	useEffect(()=>{
			if((vehicleType !== "" && makeOfVehicle !== "" && chassisNumber !== "" && plateNo !== "")){
				setIsActive(true);
			}
			else{
				setIsActive(false);
			}
		},[vehicleType, makeOfVehicle, chassisNumber, plateNo])


	return(

			<>
					<div className="container-fluid">	
						<h1 className=" text-center p-2 text-dark">Register Vehicle Here</h1>

						<div className="d-flex justify-content-center align-items-center">		
						<Form className="rounded p-2 border-1 fw-bold bg-light" onSubmit = {(e) => registerVehicle(e)}>

							<Form.Group className="p-3" controlId="firstName">
							  <Form.Label>Vehicle Type</Form.Label>
							  <Form.Control type="text" placeholder="Enter Vehicle Type" value={vehicleType} onChange={e => setVehicleType(e.target.value)}/>
							</Form.Group>

							<Form.Group className="p-3" controlId="">
							  <Form.Label>Make Of Vehicle</Form.Label>
							  <Form.Control type="text" placeholder="Enter Make OF Vehicle" value={makeOfVehicle} onChange={e => setMakeOfVehicle(e.target.value)}/>
							</Form.Group>

							<Form.Group className="p-3" controlId="mobileNo">
							  <Form.Label>Chassis Number</Form.Label>
							  <Form.Control type="number" placeholder="Enter Chassis Number" value={chassisNumber} min="0" onChange={e => setChassisNumber(e.target.value)}/>
							</Form.Group>

						      <Form.Group className="p-3" controlId="userPlateNumber">
						        <Form.Label>Plate Number</Form.Label>
						        <Form.Control type="platenumber" placeholder="Enter Plate Number" value={plateNo} onChange={e => setPlateNo(e.target.value)}/>
						      </Form.Group>

						  <MDBCheckbox name='flexCheck' value='' label='By clicking Register, you agree to our ' required/>Terms and Privacy Policy. <br/><br/>
					      
					      {
					      	isActive
					      	?
					      		<div className="d-grid gap-2">
					      		<Button variant="success" type="submit" id="submitBtn">
					      		  Register Now
					      		</Button>
					      		</div>
					      	:
					      		<div className="d-grid gap-2">

					      		<Button variant="primary" type="submit" size="sm" id="submitBtn" disabled>
					      		  Register Now
					      		</Button>
					      		</div>
					      }

					    <Button as={Link} to="/" type="submit" variant="danger" id="submitBtn" size="sm" className="mt-1">
						  Cancel
						</Button>


					    </Form>
					    </div>
					</div>
			</>
		)
}