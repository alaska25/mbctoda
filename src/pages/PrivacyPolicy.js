import { Container } from "react-bootstrap";
export default function PrivacyPolicy() {
	return(
		<>
		<Container>
			<h1 className="p-1">PRIVACY POLICY</h1>
			<p>RideMED is there to connect people through transportation and bring communities together. Our privacy policy, describe information we receive from RideMED passengers and drivers, and how we use it to connect passengers with drivers and continue to improve our services.</p>

			<h3>Scope of this Privacy Policy</h3>
			<p>rideMED (RideMED,” “we,” “our,” and/or “us”) values the privacy of individuals who use our application, websites, and related services (collectively, the “RideMED Platform ”). This privacy policy (the “Privacy Policy”) explains how we collect, use, and share information from RideMED user (“Users”), comprised of both RideMED Passengers (“Passengers”) and RideMED drivers (including Driver applicants) (“Drivers”).</p>

			<h3>Information We Collect</h3>
			<li>Registration Information. When you sign up for a RideMED account, you give us your name, email address, and phone number.</li>
			<li>Driver Application Information. If you decide to join our RideMED driver community, in addition to the basic registration information we ask you for your date of birth, physical address, national identity number, driver’s license information, vehicle information, and etc. We share this information with our partners who help us by running background checks on Drivers to help protect the rideMED community.</li>

			<h3>How We Use the Information We Collect</h3>
			<p>We use the information we collect from all Users to:</p>
			<li>Provide, improve, expand, and promote the RideMED Platform.</li>
			<li>Provide you with customer support.</li>
			<li>Find and prevent fraud.</li>
			<li>Respond to safety issues that may arise, including tricycle incidents, disputes between Passengers and Drivers, and requests from government authorities.</li>
		</Container>
		</>
		)
}