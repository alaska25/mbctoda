
import React from 'react';
import {CDBSidebar, CDBSidebarContent, CDBSidebarHeader, CDBSidebarFooter, CDBSidebarMenu, CDBSidebarMenuItem} from 'cdbreact';
import {NavLink, Link} from 'react-router-dom'; 
import {} from 'react-router-dom';

const Sidebar=()=>{
    return (
        <div style={{display:'flex', height:'100%', overflow:'scroll initial'}}>
            <CDBSidebar textColer="#fff" backgroundColor="rgb(37, 90, 122)">
                <CDBSidebarHeader prefix={<i className="fa fa-bars fa-large"></i>}>
                    <Link to="/dashboard">My Account</Link>
                </CDBSidebarHeader>
                <CDBSidebarContent className="sidebar-content">
                    <CDBSidebarMenu>
                        <NavLink exact to="/dashboard" activeClassName="activeClicked">
                            <CDBSidebarMenuItem icon="columns">
                                Ride Tricycle
                            </CDBSidebarMenuItem>
                        </NavLink>
                        <NavLink exact to="/dashboard" activeClassName="activeClicked">
                            <CDBSidebarMenuItem icon="columns">
                                Ride Motorcycle
                            </CDBSidebarMenuItem>
                        </NavLink>
                        <NavLink exact to="/dashboard" activeClassName="activeClicked">
                            <CDBSidebarMenuItem icon="columns">
                                Rent A Car
                            </CDBSidebarMenuItem>
                        </NavLink>
                        <NavLink exact to="/dashboard" activeClassName="activeClicked">
                             <CDBSidebarMenuItem icon="columns">
                                Settings
                            </CDBSidebarMenuItem>
                        </NavLink>
                    </CDBSidebarMenu>
                </CDBSidebarContent>
                <CDBSidebarFooter style={{textAlign:'center'}}>
                    <div className="sidebar-btn-wrapper" style={{ padding :'100% 5px' }}>
                        
                    </div>
                </CDBSidebarFooter>
            </CDBSidebar>
        </div>
    )
}

export default Sidebar;

