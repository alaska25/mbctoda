import { Carousel } from "react-bootstrap";
import img11 from "./images/map5.png";
import img1 from "./images/carousel1.png";
import img2 from "./images/carousel2.png";
import img3 from "./images/carousel3.png";
import img4 from "./images/carousel4.png";
export default function Header() {
	return(
		<>	
          
                    <span className="fw-bold tablink text-light p-1" style={{backgroundImage: 'linear-gradient(#0952b8,#5729f0)'}}><marquee behavior="slow" direction="left">WELCOME TO RIDEMED! BE OUR PARTNERS TODAY ... ENJOY FASTER AND RELIABLE SERVICES!</marquee></span>
                    <Carousel fade>
                          <Carousel.Item>
                            <img
                              className="d-block w-100 card-image square"
                              src={img1}
                              alt="First slide"
                            />
                          </Carousel.Item>
                          <Carousel.Item>
                            <img
                              className="d-block w-100 card-image square"
                              src={img2}
                              alt="Second slide"
                            />

                          </Carousel.Item>
                          <Carousel.Item>
                            <img
                              className="d-block w-100 card-image square"
                              src={img3}
                              alt="Third slide"
                            />
                          </Carousel.Item>
                          <Carousel.Item>
                            <img
                              className="d-block w-100 card-image square"
                              src={img4}
                              alt="Third slide"
                            />
                          </Carousel.Item>
                        </Carousel>


          </>
		            	
		)
}