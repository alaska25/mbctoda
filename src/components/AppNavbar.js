import UserContext from "../UserContext";
import {useState, useContext} from "react";
import { Container, Nav, Navbar, NavDropdown, Button } from "react-bootstrap"; 
import {Link} from "react-router-dom";
import Dropdown from 'react-bootstrap/Dropdown';
import DropdownButton from 'react-bootstrap/DropdownButton';
import { faHome, faUser, faLocationDot, faAddressBook, faAddressCard, faPhone, faUsers, faGears, facharttreemap, faDashboard, faTableList, faRightFromBracket } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import img1 from "./images/logo.png"

export default function AppNavbar(){

	const {user} = useContext(UserContext);

	return(
				    <Navbar sticky="top" collapseOnSelect expand="sm" variant="dark" className="justify-content-between" id="bg-danger">
				    <div className="container">
				    <Nav.Link as={Link} to="/" eventKey="/" className="text-light fw-bold"><Button id="button_nav"><img src={img1} alt="logo"/></Button></Nav.Link>	         	
				    <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
	   
	            <Navbar.Collapse id="responsive-navbar-nav">
	              	<Nav className="me-auto" defaultActiveKey="/">	              	
	           	</Nav>
	            <Nav className="text-center p-3">
	            	<Nav.Link as={Link} to="/" eventKey="/" className="text-light mt-2"><FontAwesomeIcon icon={faHome} className="ms-1"></FontAwesomeIcon>&nbsp;Home</Nav.Link>
	              	{
	              		(user.isAdmin)
	              			? 
	              			
			              		<Nav.Link as={Link} to="/admindashboard" eventKey="/admindashboard" className="text-light" active ><FontAwesomeIcon icon={faDashboard} className="ms-1"></FontAwesomeIcon>&nbsp;AdminDashboard</Nav.Link>            
			              	: 
				              	""
				        
	              	}
	              	
	                {	
	                (user.id !== null)
	                    ?
	                    	<Nav.Link as={Link} to="/dashboard" eventKey="/dashboard" className="text-light mt-2"><FontAwesomeIcon icon={faUser} className="ms-1 "></FontAwesomeIcon>&nbsp;My Account</Nav.Link>                  
	        			: 
		                	<>	
	                			<Nav.Link>  
	                			<DropdownButton
	                			         align={{ md: 'end' }}
	                			         title="Be Our Partner"
	                			         id="dropdown-menu-align-responsive-1"
	                			       >
	                			         <Dropdown.Item as={Link} to="/register_driver" eventKey="/register_driver" className="text-start">Driver</Dropdown.Item>
	                			         <Dropdown.Item as={Link} to="/register_passenger" eventKey="/register_passenger" className="text-start">Passenger</Dropdown.Item>
	                			         <Dropdown.Item as={Link} to="/register_vehicle" eventKey="/register_vehicle" className="text-start">Vehicle</Dropdown.Item>
	                			</DropdownButton>
	                			</Nav.Link> 

	                			<Nav.Link as={Link} to="/login" eventKey="/login"className="text-light"></Nav.Link> 	         	
		                	</>
	                }
	                {
	                	(user.isDriver)
	                	?
	                		<Nav.Link as={Link} to="/driver_dashboard" eventKey="/dashboard" className="text-light mt-2"><FontAwesomeIcon icon={faUser} className="ms-1 "></FontAwesomeIcon>&nbsp;My Account</Nav.Link>

	                	:
	                		""
	                }

	            </Nav>
	            </Navbar.Collapse>
	            </div>
	        </Navbar>
		)

}

