import {Row, Col, Button, Container, Card} from "react-bootstrap";
import {Link} from "react-router-dom";

export default function Banner({data}){
	// Destructuring refer to errorjs
	const {title, content, destination, label} = data;

	return(
    <>
	       <Col className="p-3 text-center">
					
					
					<h4 className="fw-bold" id="banner">Better Solution For <br/> Your Trip And Businesses</h4>
					<Button as={Link} to="/driver_list" eventKey="/Driver_List" className="fw-bold">Book Now</Button>
					
	       </Col>
    		
	</>
	)
}
